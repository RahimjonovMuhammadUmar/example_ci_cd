package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/hello", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "hello world",
		})
	})
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "This is ci cd example app.",
		})
	})
	fmt.Println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nchanged")
	r.Run(":8000")
}
